import math

def mean(l1,l2):
    #finding mean
    centroid1=sum(l1)/len(l1)
    centroid2=sum(l2)/len(l2)
    return math.ceil(centroid1),math.ceil(centroid2)

def create_cluster(l,centroid1,centroid2,L1):
    #create clusters
    cluster1=[]
    cluster2=[]
    #making sure that centroid1 is always greater than centroid2
    if centroid1<centroid2:
        centroid1,centroid2=centroid2,centroid1
    for x in l:
        if abs(x-centroid1)<=abs(x-centroid2):
            #appending into the cluster having max marks
            cluster1.append(x)
        else:
            #appending into the cluster having min marks
            cluster2.append(x)
    #again calculate centroids using new clusters
    new_centroid1,new_centroid2=mean(cluster1,cluster2)
    #recursion until new and previous centroids are equal
    if new_centroid1==centroid1 and new_centroid2==centroid2:
        print(cluster1,cluster2)
        L1.append(min(cluster1))
        return centroid1,centroid2
    return create_cluster(l,new_centroid1,new_centroid2,L1)


if __name__ == '__main__':
    l=[[1,2,3,0,2,3,2,2,10,12,9,14,17,6,3],[11,13,17,21,12,9,0,6,3],[1,3,3,0,5,7,9,5]]
    L1=[]
    print("The Clusters are")
    for i in l:
        #setting the initial centroids as min and max of the list
        centroid1,centroid2=min(i),max(i)
        res1,res2=create_cluster(i,centroid1,centroid2,L1)
    print("Minimum of 1st clusters ",L1)
